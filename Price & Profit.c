#include <stdio.h>
#include <stdlib.h>

int Attendees(int tp){return 120-(tp-15)/5*20;}
int income(int tp){return Attendees(tp)*tp; }
int cost(int tp){return Attendees(tp)*3+500; }
int profit(int tp){return income(tp)- cost(tp); }
int main()
{
    int tp;
    printf("Ticket price & Profit\n\n");
     printf("Ticket price \t Profit\n");
      printf("   Rs \t           Rs\n\n");
    for(tp=5;tp<=50;tp=tp+5)
    {

        printf("   %d \t          %d\n\n",tp,profit(tp));

    }

    return 0;
}
